<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/admin', function () {
    return view('layout.master');
});

Route::get('/login', function () {
    return view('layout.login');
});

Route::get('/table', function () {
    return view('layout.partials.table');
});

Route::get('/data-tables', function () {
    return view('layout.partials.data-tables');
});

Route::get('/dashboard', function () {
    return view('layout.partials.dashboard');
});